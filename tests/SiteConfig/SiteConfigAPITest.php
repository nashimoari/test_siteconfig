<?php

namespace Tests\Unit;

use App\Facades\SiteConfigAtJSON;
//use App\Services\SiteConfig\SiteConfigFactory;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class SiteConfigAPITest extends TestCase
{

    public function testA1()
    {
        //SiteConfigAtJSON::all();
        SiteConfigAtJSON::set('myAwesomeTestSetting1','It is work!');

        //print_r(Config::all()); //get('app.timezone');
        //$response = $this->get('/api/siteConfig');

        $this->assertTrue(true,'true');
        //$response->assertStatus(200);
        //echo $response->content();
        //var_dump($response);
    }

    /**
     * Test return all variables.
     *
     * @return void
     */
    public function testAll()
    {
        $response = $this->get('/api/siteConfig/all');

        $response->assertStatus(200);
        echo $response->content();
        //var_dump($response);
    }

    public function testHas()
    {
        $response = $this->get('/api/siteConfig/has/myAwesomeTestSetting1');

        $response->assertStatus(200);
        echo $response->content();
        //var_dump($response);
    }

    public function testGet()
    {
        $response = $this->get('/api/siteConfig/get/myAwesomeTestSetting1');

        $response->assertStatus(200);
        echo $response->content();
        //var_dump($response);
    }

    public function testSet()
    {
        $response = $this->post('/api/siteConfig/set/myAwesomeTestSetting2', ['value'=> 'test value']);

        $response->assertStatus(200);
        echo $response->content();
        //var_dump($response);
    }


    public function testSetArr()
    {
        $response = $this->post('/api/siteConfig/set/myAwesomeTestArr', ['value'=> 'test value 1']);

        $response->assertStatus(200);
        echo $response->content();
        //var_dump($response);
    }

    public function testPush()
    {
        $response = $this->post('/api/siteConfig/push/myAwesomeTestArr', ['value'=> 'test value 3']);

        $response->assertStatus(200);
        echo $response->content();
    }

    public function testPrepend()
    {
        $response = $this->post('/api/siteConfig/prepend/myAwesomeTestArr', ['value'=> 'test value start']);

        $response->assertStatus(200);
        echo $response->content();
    }



}
