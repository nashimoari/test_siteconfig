<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
})
;

//Route::apiResource('/siteConfig', 'SiteConfigController');

Route::prefix('siteConfig')->group(function () {
    Route::get('all', 'SiteConfigController@all');
    Route::get('has/{settingName}', 'SiteConfigController@has');
    Route::get('get/{settingName}', 'SiteConfigController@get');
    Route::post('set/{settingName}', 'SiteConfigController@set');
    Route::post('prepend/{settingName}', 'SiteConfigController@prepend');
    Route::post('push/{settingName}', 'SiteConfigController@push');
});
