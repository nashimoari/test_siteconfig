<?php


namespace App\Services\SiteConfig\Repository;

use App\Services\SiteConfig\Repository\AbstractRepository;


class JSONRepository extends AbstractRepository
{
    private $settingsPath;

    public function __construct()
    {
        $storagePath = storage_path();
        $this->settingsPath = $storagePath . '/Settings.json';

        parent::__construct();
    }

    protected function importSettings()
    {
        if (file_exists($this->settingsPath)) {
            $this->items = json_decode(file_get_contents($this->settingsPath), true);
        } else {
            $this->items = [];
            $this->exportSettings();
        }

    }


    protected function exportSettings()
    {
        $fp = fopen($this->settingsPath, 'w');
        fwrite($fp, json_encode($this->items));
        fclose($fp);
    }
}
