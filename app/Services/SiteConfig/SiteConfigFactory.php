<?php


namespace App\Services\SiteConfig;


use App\Services\SiteConfig\Repository\JSONRepository;

class SiteConfigFactory
{
    Public static function GetInstance($type)
    {
        if ($type == 'JSON') {
            return new JSONRepository();
        }

        throw new Exception('Source type {$type} is absent for this type');
    }
}
