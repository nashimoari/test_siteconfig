<?php

namespace App\Http\Controllers;

use App\Http\Requests\SiteConfigRequest;
use App\Facades\SiteConfigAtJSON;
use Illuminate\Http\Request;


class SiteConfigController extends Controller
{
    public function all(SiteConfigRequest $request)
    {
        return SiteConfigAtJSON::all();
    }

    public function has(SiteConfigRequest $request,$settingName)
    {
        return [SiteConfigAtJSON::has($settingName)];
    }

    public function get(SiteConfigRequest $request,$settingName)
    {
        return SiteConfigAtJSON::get($settingName);
    }

    public function set(SiteConfigRequest $request,$settingName)
    {
        return SiteConfigAtJSON::set($settingName,$request->post('value'));
    }

    public function prepend(SiteConfigRequest $request,$settingName)
    {
        return SiteConfigAtJSON::prepend($settingName,$request->post('value'));
    }

    public function push(SiteConfigRequest $request,$settingName)
    {
        return SiteConfigAtJSON::push($settingName,$request->post('value'));
    }
}
