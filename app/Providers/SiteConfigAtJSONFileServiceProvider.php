<?php

namespace App\Providers;

use App\Services\SiteConfig\SiteConfigFactory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;

class SiteConfigAtJSONFileServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('siteconfig.json', function () {
            return SiteConfigFactory::GetInstance('JSON');
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
